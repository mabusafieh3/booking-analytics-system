﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Booking_System
{
    public class Record
    {
        public Record(string person, string iSBN, DateTimeOffset timestamp, string action)
        {
            Person = person;
            ISBN = iSBN;
            Action = action;
            Timestamp = timestamp;
            SetTicks(ticks);
        }

        public string Person { get; set; }

        public string ISBN { get; set; }

        public DateTimeOffset Timestamp { get; set; }

        public string Action { get; set; }

        private long ticks;

        public long GetTicks()
        {
            return Timestamp.Ticks;
        }

        public void SetTicks(long value)
        {
            ticks = value;
        }
    }
    class Program
    {
        private static long total = 0L;
        private static string lastNodeAction = "";
        static void Main(string[] args)
        {
            string inputFilePath;
            string outputFileType;

            Console.WriteLine("\n============== Starting Program ===============\n");

            /*------------------ reading user input ------------------*/
            Console.Write("Enter input file path: ");
            inputFilePath = Console.ReadLine();
            Console.Write("If you need the output file in JSON type 'J', otherwise the output file will be exported as a text file (Optional): ");
            outputFileType = Console.ReadLine();

            /*------------------ reading input file ------------------*/
            List<Record> query = readingInputFile(inputFilePath);

            /*------------------ handle statistical operations ------------------*/
            var resultJson = handleStatisticalData(query);


            /*------------------ write output results to file ------------------*/
            handelOutoutFile(resultJson, outputFileType);

            /*------------------ write output results to console ------------------*/
            Console.WriteLine("\n=============================\n");
            Console.WriteLine(resultJson);
            Console.WriteLine("\n=============================\n");

            Console.WriteLine("\n============== Ending Program ===============\n");
        }

        public static List<Record> readingInputFile(string inputFilePath)
        {
            List<Record> query = new List<Record>();
            if (!string.IsNullOrEmpty(inputFilePath))
            {
                if (inputFilePath.EndsWith(".xml"))
                {
                    XElement root = XElement.Load(inputFilePath);

                    query =
                        (from el in root.Elements("record")
                         select new Record(el.Element("person").Attribute("id").Value,
                                    el.Element("isbn").Value,
                                    (DateTimeOffset)el.Element("timestamp"),
                                    el.Element("action").Attribute("type").Value)).ToList();
                }
                else if (inputFilePath.EndsWith(".csv"))
                {
                    query = File.ReadLines(inputFilePath)
                                .Skip(1)
                                .Where(s => s != "")
                                .Select(s => s.Split(new[] { ',' }))
                                .Select(a => new Record(a[1], a[2], DateTimeOffset.Parse(a[0]), a[3]))
                                .ToList();
                }
                else
                {
                    // Console app
                    Console.WriteLine("Wrong entry.");
                    Console.WriteLine("\n============== Ending Program ===============\n");
                    System.Environment.Exit(1);
                }
            }
            else
            {
                // Console app
                Console.WriteLine("Wrong entry.");
                Console.WriteLine("\n============== Ending Program ===============\n");
                System.Environment.Exit(1);
            }

            return query;
        }

        public static Object handleStatisticalData(List<Record> query)
        {
            /*person has the most checkouts*/
            var personWithMostCheckouts =
                (from el in query
                 where el.Action == "check-out"
                 group el by el.Person into g1
                 orderby g1.Count() descending
                 select g1.Key).First();

            /*Which book was checked out the longest time in total*/
            var groupByBook =
                from el in query
                group el by el.ISBN into g2
                select new
                {
                    key = g2.Key,
                    book_checkout_count = g2.Count(),
                    book_checkout_time = g2.Aggregate(0L, func: sumUpCheckoutTime, getCheckoutTotalTime)
                };

            var mostCheckedOutBook =
                (from elm in groupByBook
                 orderby elm.book_checkout_time descending
                 select elm.key).First();

            /*How many books are checked out at this moment */
            var currentCheckedOutBookCount =
                (from el in query
                 select el).Aggregate(0,
                    (current, next) =>
                        next.Action == "check-in" ? current - 1 : current + 1,
                    res => res);

            /*Who currently has the largest number of books */
            var personHasCurrentlyMostBooks =
                ((from el in query
                  group el by el.Person into g2
                  select new
                  {
                      key = g2.Key,
                      book_max = g2.Aggregate(0,
                      (longest, next) =>
                          next.Action == "check-in" ? longest - 1 : longest + 1,
                      res => res)
                  }).OrderByDescending(order => order.book_max)).First().key;

            /* write to result obj*/
            var resultJson = new
            {
                person_with_most_checkouts = personWithMostCheckouts,
                most_checked_out_book = mostCheckedOutBook,
                current_checked_out_book_count = currentCheckedOutBookCount,
                person_who_has_currently_most_books = personHasCurrentlyMostBooks
            };

            return resultJson;
        }

        public static void handelOutoutFile(Object resultJson, string outputFileType)
        {
            var outputFileName = "output_" + DateTime.Now.ToString("yyyy-MM-dd_hh-mm-ss");
            if (outputFileType == "J")
            {
                outputFileName += ".json";
            }
            else
            {
                outputFileName += ".txt";
            }
            File.WriteAllText(outputFileName, resultJson.ToString());
        }
        public static long sumUpCheckoutTime(long current, Record next)
        {
            long result = next.GetTicks();
            lastNodeAction = next.Action;
            if (next.Action == "check-in")
            {
                result = next.GetTicks() - current;
                total += result;
            }
            return result;
        }

        private static long getCheckoutTotalTime(long result)
        {
            long temp = total;
            if (lastNodeAction == "check-out")
            {
                temp = DateTime.Now.Ticks - result;
                temp += total;
            }
            total = 0L;
            return temp;
        }
    }
}